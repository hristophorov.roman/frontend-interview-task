const routes = {
    home: {
        path: '/',
        name: 'Home',
    },
    login: {
        path: '/login',
        name: 'Login',
    },
    confirm: {
        path: '/confirm',
        name: 'Confirm',
    },
}
export { routes }
