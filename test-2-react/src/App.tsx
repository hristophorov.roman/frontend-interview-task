import { Route, Switch } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { routes } from './config/routes'
import LoginPage from './pages/Login'
import HomePage from './pages/Home'
import ConfirmPage from './pages/Confirm'
import NotFoundPage from './pages/404'

export default function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path={routes.home.path} component={HomePage} />
                <Route path={routes.login.path} component={LoginPage} />
                <Route path={routes.confirm.path} component={ConfirmPage} />
                <Route path="*" component={NotFoundPage} />
            </Switch>
        </BrowserRouter>
    );
}
