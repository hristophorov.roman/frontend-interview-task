import React, { useState, useEffect, useCallback, ChangeEvent } from 'react'
import { useHistory } from 'react-router-dom'
import { routes } from '../config/routes'
import { Header } from './Header'
import { FormCheckbox } from './Componets/Checkbox'
import { FormInput } from './Componets/FormInput'
import { Button } from './Componets/Button'

const ValidEmailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/

function LoginPage() {
    const history = useHistory();
    const [email, setEmail] = useState<string>('');
    const [isValid, setEmailValid] = useState<boolean>(false);
    const [isChecked, setIsChecked] = useState<boolean>(false);

    useEffect(() => {
        const savedEmail = sessionStorage.getItem('validEmail');
        if (savedEmail) {
            setEmail(savedEmail);
            setEmailValid(true);
        }
    }, []);

    const handleEmail = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        const emailValue = e.target.value;
        setEmail(emailValue);

        if (ValidEmailRegex.test(emailValue)) {
            setEmailValid(true);
            sessionStorage.setItem('validEmail', emailValue);
        } else {
            setEmailValid(false);
        }
    }, []);

    const handleCheckbox = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setIsChecked(event.target.checked);
    }, []);

    const handleHoldComplete = useCallback(() => {
        history.push(routes.confirm.path);
    }, []);

    return (
        <>
            <Header name={routes.login.name} />
            <main className="flex flex-col p-4 h-full">
                <FormInput email={email} onChange={handleEmail} />
                <div className="p-1"></div>
                <FormCheckbox onChange={handleCheckbox} isChecked={isChecked} />
                <Button
                    isDisabled={!(isValid && isChecked)}
                    onHoldComplete={handleHoldComplete}
                />
            </main>
        </>
    )
}

export default LoginPage
