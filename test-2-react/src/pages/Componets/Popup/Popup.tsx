import React from 'react'
import './Popup.css'

type Props = {
    onClick: () => void;
    message: string;
}

export const Popup: React.FC<Props> = React.memo(({
    message, onClick,
}) => {
    return (
        <div className={`popup popup-${message}`}>
            <h2>{message}!</h2>
            <button 
                className="btn btn-primary"
                onClick={onClick}
            >
                {`<`}
            </button>
        </div>
    )
})
