import React, { ChangeEvent } from 'react'

type Props = {
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
    email: string;
}

export const FormInput: React.FC<Props> = React.memo(({
    onChange,
    email,
}) => {
    return (
        <label className="form-control">
            <div className="label">
                <span className="label-text">Email</span>
            </div>
            <input 
                className="input"
                type="text" 
                placeholder="Type here"
                value={email}
                onChange={onChange}
            />
        </label>
    )
})
