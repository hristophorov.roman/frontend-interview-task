import React, {useState, useEffect, useRef} from 'react'

type Props = {
    onHoldComplete: () => void;
    isDisabled: boolean;
}

export const Button: React.FC<Props> = React.memo(({
    onHoldComplete,
    isDisabled,
}) => {
    const [timeHeld, setTimeHeld] = useState(0);
    const timerId = useRef(null);
    const holdDuration = 500;

    useEffect(() => {
        if (timeHeld >= holdDuration) {
            onHoldComplete();
            setTimeHeld(0);
        }
    }, [timeHeld, onHoldComplete]);

    const startHold = () => {
        // @ts-expect-error
        timerId.current = setInterval(() => {
            setTimeHeld(prev => prev + 10);
        }, 10);
    };

    const endHold = () => {
        if (timerId.current) {
            clearInterval(timerId.current);
        }
        setTimeout(() => setTimeHeld(0), 1000);
    };

    return (
        <button
            className="btn btn-primary mt-auto"
            onMouseDown={startHold}
            onMouseUp={endHold}
            onMouseLeave={endHold}
            disabled={isDisabled}
        >
            {timeHeld ? `${(timeHeld / 1000)}s` : 'Hold to proceed'}
        </button>
    )
})
