import React, {ChangeEvent} from 'react'

type Props = {
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
    isChecked: boolean;
}

export const FormCheckbox: React.FC<Props> = React.memo(({
    onChange, isChecked
}) => {
    return (
        <div className="form-control">
            <label className="label cursor-pointer justify-start gap-2">
                <input
                    className="checkbox checkbox-primary"
                    type="checkbox"
                    checked={isChecked}
                    onChange={onChange}
                />
                <span className="label-text">I agree</span>
            </label>
        </div>
    )
})
