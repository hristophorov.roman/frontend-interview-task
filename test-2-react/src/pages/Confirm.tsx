import { useState, useCallback } from 'react'
import { useHistory } from 'react-router-dom'
import { Header } from './Header'
import { routes } from '../config/routes'
import { Popup } from './Componets/Popup'

function ConfirmPage() {
    const history = useHistory();
    const email = sessionStorage.getItem('validEmail') || '';
    const [message, setMessage] = useState<string>('');

    const handleBack = () => {
        history.goBack();
    };

    const handleClosePopup = useCallback(() => {
        setMessage('')
    }, []);

    const handleConfirmClick = async () => {
        try {
            const response = await fetch('api/endpoint', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email }),
            });

            if (response.ok) {
                setMessage('Success')
            } else {
                setMessage('Error')
            }
        } catch (e) {
            setMessage(`An error occurred while sending the request: ${e}`);
        }
    };

    return (
        <>
            <Header name={routes.confirm.name} />
            <main className="flex flex-col p-4 h-full">
                {!message && 
                <> 
                    <input className="input" 
                        disabled={Boolean(email)}
                        defaultValue={email}
                    />
                    <div className="p-1"></div>
                    <button className="btn btn-active mt-auto"
                        onClick={handleBack}
                    >
                        Back
                    </button>
                    <button className="btn btn-primary"
                        onClick={handleConfirmClick}
                    >
                        Confirm
                    </button>
                </>}
                {message && (
                    <Popup 
                        message={message}
                        onClick={handleClosePopup}
                    />
                )}
            </main>
        </>
    )
}

export default ConfirmPage
