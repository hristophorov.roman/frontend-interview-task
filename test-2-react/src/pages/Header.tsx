export const Header = ({ name }: { name: string }) => {
    return (
        <header className="h-20 bg-primary flex items-center p-4">
            <h1 className="text-3xl text-black">{name}</h1>
        </header>
    )
}
