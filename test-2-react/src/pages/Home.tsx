import { Link } from 'react-router-dom'
import { routes } from '../config/routes'
import { Header } from './Header'

function HomePage() {
    return (
        <>
            <Header name={routes.home.name} />
            <Link to={routes.login.path}>Click and go o to Login page:)</Link>
        </>
    )
}

export default HomePage
