import { Header } from "./Header"

function NotFoundPage() {
    return (
        <>
            <Header name={'404'} />
            <h1 className="text-3xl text-black">Page not found</h1>
        </>
    )
}

export default NotFoundPage
